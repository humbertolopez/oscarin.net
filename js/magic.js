$(document).ready(function(){
	$('.masonry').masonry({
		itemSelector: '.col-lg-2',
		columnWidth: '.col-lg-2',
		stamp: '.title',
		percentPosition: true
	});
	$('.masonry6').masonry({
		itemSelector: '.col-lg-6',
		columnWidth: '.col-lg-6',
		//stamp: '.title',
		percentPosition: true
	});
});